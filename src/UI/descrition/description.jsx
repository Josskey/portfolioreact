import React from "react";
import './description.css'
import Link from "../links/link";

const Description = (props)=>{
    return (
        <div className="description">
            
            {props.link === true 
            ? 
            <div>
                <h3>Мои контакты</h3>
                <div className="links">
                    <Link icon='telegram' href='https://t.me/d33mas'></Link>
                    <Link icon='vk' href='https://vk.com/d33ma'></Link>
                </div>
            </div>
            : <div></div>}

            {props.text === true
            ?
            <div>
                <h3>{props.name}</h3>
                <div className="descriptionText">
                    <p className="Text">{props.children}</p>
                </div>
            </div>
            : <div></div>
            }
            
        </div>
    )
}

export default Description