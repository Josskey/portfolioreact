import React from "react";
import MainWindow from '../UI/window/window'
import Description from "../UI/descrition/description";
import MainInfo from "../UI/mainInfo/mainInfo";
import Navigation from "../UI/navigation/navigation"


const ProjectIs = (props) => (
    <div className="Project">
        <MainWindow>
            <Navigation path="/">Назад</Navigation>
            <div className="windowMain">
                <MainInfo
                    path="INST-logo.png"
                    name="Имеющиеся знания"
                    whoAmI="HTML5, CSS, SQL, Tilda, Photoshope, CorelDRAW, Haml/C#, 1C, BpWin/StarUML "
                >
                    ВсЕгО ПоНеМНогУ xD 

                </MainInfo>
                <Description link={false} text={true} name='Инфо'>
                 Все имеющиеся знания получил в универе.
                </Description>
            </div>
            <div className="windowMain">
                <MainInfo
                    path="INST-logo2.png"
                    name="В процессе"
                    whoAmI="Git, React, Js"
                >
                    Git не почувстровали 😎
                    React потненькая вещь 🥵🤯
                    Js не далеко ушел от React 
                </MainInfo>
                <Description link={false} text={true} name='Инфо'>
                 Очень информативные лекции от ESOFT, и суперские спикеры.
                </Description>
            </div>
            <div className="windowMain">
                <MainInfo
                    path="INST-logo3.png"
                    name="В будущем"
                    whoAmI="TYPESCRIPT, postgreSQL, Node.js, Docker, Тестирование"
                >
                 гОтОвЛюСь К ДиПлОмУ

                </MainInfo>
                <Description link={false} text={true} name='Инфо'>
                 Собираем команду на дипломку.
                </Description>
            </div>
            {/* <Navigation path="/about-me">Далее</Navigation> */}

        </MainWindow>
    </div>
)

export default ProjectIs