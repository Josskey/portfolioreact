import React from "react";
import MainWindow from '../UI/window/window'
import Description from "../UI/descrition/description";
import MainInfo from "../UI/mainInfo/mainInfo";
import Navigation from "../UI/navigation/navigation"


const Main = (props) => {
    return (
        <MainWindow>
            <div className="windowMain">
                <MainInfo 
                    path="photo.jpg"
                    name="Дмитрий Прокопьев"
                    whoAmI='ПРОГРАММИСТ, АДМИНИСТРАТОР БАЗ ДАННЫХ, WEB - ДИЗАЙНЕР и просто хороший человек xD'
                >
                    Хобби: Photoshop; Чтение научной литературы; Обширные познания в разделе медицины "Гастроэнтерология"
                </MainInfo>
                <Description link={true} text={true} name="Про меня">
                    Привет. Я Дмитрий. Ученик ESOFT 2024. 
                </Description>
            </div>
            <Navigation path="/project">Мои знания</Navigation>
        </MainWindow>
    )
}

export default Main